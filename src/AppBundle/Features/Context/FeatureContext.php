<?php

namespace AppBundle\Features\Context;

use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\HttpKernel\KernelInterface;


/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements KernelAwareContext
{
    /** @var  KernelInterface */
    private $kernel;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @When /^я вижу слово "([^"]*)" где\-то на странице$/
     */
    public function яВижуСловоГдеТоНаСтранице($arg1)
    {
        $this->assertPageContainsText($arg1);
    }

    /**
     * @When /^я ввожу логин "([^"]*)" пароль "([^"]*)" и нажимаю войти$/
     */
    public function яВвожуЛогинПарольНажимаюВойти($arg1, $arg2)
    {
        $page = $this->getSession()->getPage();
        $inputUsername = $page->find('css', '#username');
        $inputUsername->setValue($arg1);
        $inputUsername = $page->find('css', '#password');
        $inputUsername->setValue($arg2);
        $submit = $page->find('css', '#_submit');
        $submit->click();
    }

    protected function getContainer()
    {
        return $this->kernel->getContainer();
    }

    /**
     * @When /^я нахожусь на главной странице$/
     */
    public function яНахожусьНаГлавнойСтранице()
    {
        $this->visit($this->getContainer()->get('router')->generate('index'));
    }

    /**
     * @When /^я нахожусь на странице авторизации$/
     */
    public function яНахожусьНаСтраницеАвторизации()
    {
        $this->visit($this->getContainer()->get('router')->generate('fos_user_security_login'));
    }

    /**
     * @When /^я нахожусь на странице фраз/
     */
    public function яНахожусьНаСтраницеФраз()
    {
        $this->visit($this->getContainer()->get('router')->generate('tasks', ['_locale' => 'ru']));
    }

    /**
     * Sets Kernel instance.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @When /^я отправляю фразу "([^"]*)"$/
     */
    public function яОтправляюФразу($arg1)
    {
        $page = $this->getSession()->getPage();
        $inputPhrase = $page->find('css', '#form_ruContent');
        $inputPhrase->setValue($arg1);
        $submit = $page->find('css', '#form_save');
        $submit->click();
    }

    /**
     * @When /^я перехожу к переводу фразы$/
     */
    public function яПерехожуКПереводуФразы()
    {
        $page = $this->getSession()->getPage();
        $inputPhrase = $page->find('css', 'ul li:last-child a');
        $inputPhrase->click();
    }

    /**
     * @When /^я ввожу перевод фразы "([^"]*)" на язык "([^"]*)"$/
     */
    public function яВвожуПереводФразыНаЯзык($arg1, $arg2)
    {
        $page = $this->getSession()->getPage();
        $inputPhrase = $page->find('css', '#'.$arg2.'Content');
        $inputPhrase->setValue($arg1);
    }

    /**
     * @When /^отправляю перевод$/
     */
    public function отправляюПеревод()
    {
        $page = $this->getSession()->getPage();
        $inputSubmit = $page->find('css', 'input[type=submit]');
        $inputSubmit->click();
    }
}
