<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user1 = new User();
        $user1
            ->setUsername('admin')
            ->setEmail('original@some.com')
            ->setEnabled('1')
            ->setPlainPassword('123321')
            ->setName('Admin')
            ->setRoles(['ROLE_ADMIN']);

        $manager->persist($user1);

        $this->addReference('user1', $user1);

        $user2 = new User();
        $user2
            ->setUsername('user1')
            ->setEmail('user1@some.com')
            ->setEnabled('1')
            ->setPlainPassword('123')
            ->setName('User1')
            ->setRoles(['ROLE_USER']);

        $manager->persist($user2);

        $this->addReference('user2', $user2);

        $manager->flush();
    }
}