<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


class UserController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function indexAction()
    {
        if($this->getUser()){
            return $this->redirectToRoute('tasks');
        }
        return $this->render('@App/User/index.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/new_user", name="new_user")
     * @Method({"GET","HEAD","POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, EntityManagerInterface $em)
    {

        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $em = $this->getDoctrine()->getManager();

            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $user->getPassword()); //шифруем пароль
            $user->setPassword($password);
            $user->setRoles(['ROLE_USER']); //устанавливаем роль пользователя по умолчанию
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('app_basic_login', array('id' => $user->getId()));
        }

        return $this->render('@App/User/new.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

}
